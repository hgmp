# 0.1.2.1
- fix incorrect wrappers for Win64
- relax dependencies for ghc-9.4

# 0.1.2
- ghc-9 compatibility

# 0.1.1
- raw function foreign import bindings
- define hsc2hs #alignment macro for older GHC versions

# 0.1.0.1
- use hsc2hs's #alignment macro in Storable instances

# 0.1.0.0
- initial release
